#!/usr/bin/env bash
# Installation instructions getting a FADS nix environment up 
# 1: Install OS using Server with GUI option
# 2: Install Virtual Box "Guest Additions" from the devices list in VB menu
# 3: Register EPEL (TODO: automate)
# 3: Run this script

export NODE_VERSION=6.7
export PRIVATE_NPM_REPO=goanpm.cloudapp.net
clear

# First things first, we're always going to need this
if [ ! -d $HOME/.ssh ] ; then
    mkdir $HOME/.ssh
    chmod 0700 $HOME/.ssh
fi 

printf "\nWill this be a pesronal install? (y?)\n"
read personal_input 
personal_input=${personal_input,,} #apparently this converts it to lowercase in bash 4
personal_install=false
if [ ! -z $personal_input ] ; then
    if [ $personal_input = 'y' ] || [ $personal_input = 'yes' ] ; then 
        personal_install=true

        printf "\nIs this Ryan?\n"
        read ryan_input 
        ryan_input=${ryan_input,,} #apparently this converts it to lowercase in bash 4
        ryan_install=false
        if [ ! -z $ryan_input ] ; then
            if [ $ryan_input = 'y' ] || [ $ryan_input = 'yes' ] ; then 
                ryan_install=true
            fi
        fi

    fi
fi

###################################################################################################
## Base Image 
###################################################################################################


if [ ! -z $base_dev ] ; then
    if [ $base_dev = true ] ; then
        sudo yum update -y && sudo yum upgrade -y
        sudo yum groups mark install "Development Tools" -y 
        sudo yum groupinstall 'Development Tools' -y
        sudo yum install bash  -y
        sudo yum install git  -y
        sudo yum install cmake  -y
        sudo yum install make -y
        <<<<<<< HEAD

        sudo yum install elementary-icon-theme -y
        sudo yum install evolution -y
        sudo yum install gnome-themes-standard -y
        sudo yum install gnome-tweak-tool -y
        sudo yum install libglib2.0-bin -y
        sudo yum install slim -y
        sudo yum install bison -y
        sudo yum install flex -y
        sudo yum install llvm -y
        sudo yum install nginx -y
        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash 
        source ~/.nvm/nvm.sh
        nvm install $NODE_VERSION;
        echo "exec startxfce4" > $HOME/.xinitrc
        gsettings set org.gnome.desktop.background show-desktop-icons false

        sudo yum install elementary-icon-theme -y
        sudo yum install gnome-themes-standard -y
        sudo yum install gnome-tweak-tool -y
        sudo yum install lxappearance -y
        sudo yum install bison -y
        sudo yum install flex -y
        sudo yum install llvm -y
        sudo yum install nginx -y
        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash 
        source ~/.nvm/nvm.sh
        nvm install $NODE_VERSION;
        echo "exec startxfce4" > $HOME/.xinitrc
        gsettings set org.gnome.desktop.background show-desktop-icons false

        npm config set registry http://goanpm.cloudapp.net
        npm install vienna

        #setup reverse proxy vienna
        sudo echo "
        server {
        listen 80;
        server_name fads.gov.ab.ca;
        location / {
        proxy_pass http://APP_PRIVATE_IP_ADDRESS:8000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
" > /etc/nginx/sites-available/default 

    fi
fi


if [ $personal_install = true ] ; then 
    printf "Personal Install"

    sudo yum install openssl -y 
    sudo yum install pinentry-curses -y
    sudo yum install tree  -y
    sudo yum install wget  -y
    sudo yum install curl -y

    if [ ! -d $HOME/.tmp ] ; then 
        mkdir $HOME/.tmp
    fi

    if [ ! -d $HOME/.fonts ] ; then 
        mkdir $HOME/.fonts
    fi

    # Chrome Install
    cd $HOME/.tmp
    wget https://download-chromium.appspot.com/dl/Linux_x64?type=snapshots -O chrome_snapshot.rpm
    rpm -i chrome_snapshot.rpm


    #TODO Set EPEL Programaticaly
    sudo yum install ncurses-devel luajit

    sudo yum remove vim vim-runtime vim-gnome vim-tiny vim-common vim-gui-common -y
    sudo rm -rf /usr/local/share/vim
    sudo rm /usr/bin/vim
    sudo mkdir /usr/include/lua5.1/include
    sudo mv /usr/include/lua5.1/*.h /usr/include/lua5.1/include/
    sudo ln -s /usr/bin/luajit-2.0.0-beta9 /usr/bin/luajit

    cd $HOME/code/current
    git clone https://github.com/vim/vim
    cd vim/src
    make distclean
    ./configure --bindir=/usr/bin \
        --enable-cscope \
        --enable-fail-if-missing \
        --enable-largefile \
        --enable-luainterp \
        --enable-luainterp=yes \
        --enable-multibyte \
        --enable-pythoninterp \
        --enable-pythoninterp=yes \
        --prefix=/usr \
        --with-features=huge \
        --with-lua-prefix=/usr/include/lua5.1 \
        --with-luajit 
    sudo make 
    sudo make install


    # Font Awesome
    cd $HOME/.tmp/
    git clone https://github.com/FortAwesome/Font-Awesome.git
    cp Font-Awesome/fonts/*.ttf $HOME/.fonts

    cd $HOME/.tmp
    wget https://github.com/primer/octicons/archive/v4.3.0.zip
    unzip v4.3.0.zip 
    cp ./octicons-4.3.0/build/font/*.ttf $HOME/.fonts

    cd $HOME/.tmp
    git clone https://github.com/hbin/top-programming-fonts.git
    cp ./top-programming-fonts/*.ttf $HOME/.fonts

    cd $HOME/.tmp
    wget https://vaadin.com/download/vaadin-icons/vaadin-icons.zip
    unzip vaadin-icons.zip 
    cp ./fonts/*.ttf $HOME/.fonts

    cd /usr/share/themes
    sudo git clone https://github.com/axxapy/Adwaita-dark-gtk2.git Adwaita-dark

fi

if [ $ryan_install = true ] ; then 

    cd $HOME
    git clone https://github.com/ryan-thompson/bin.git
    git clone https://github.com/ryan-thompson/.dotfiles.git
    git clone https://github.com/ryan-thompson/.vim.git

    cd $HOME

    files=$HOME/.dotfiles/*
    home=$HOME

    for f in $files
    do
        filename=$(basename $f)
        printf "deleting .$filename\n"
        rm -f $home/.$filename
        printf "linking $filename\n"
        ln -s $f $home/.$filename
        printf "--------------------------\n"
    done

    rm -rf $HOME/.local/share/xfce4/terminal/colorschemes  
    mkdir -p $HOME/.local/share/xfce4/terminal/colorschemes  
    ln -s $HOME/.dotfiles/pherocity.theme $HOME/.local/share/xfce4/terminal/colorschemes/pherocity.theme

    rm $HOME/.config/xfce4/terminal/terminalrc 
    cp -s $HOME/.dotfiles/terminalrc $HOME/.config/xfce4/terminal/terminalrc

    cd $HOME
    vim +PluginInstall +qall
    uas
fi


